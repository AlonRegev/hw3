#include "Vector.h"

Vector::Vector(int n)
{
	if (n < MIN_CAP) {
		n = MIN_CAP;
	}
	this->_size = 0;
	this->_capacity = n;
	this->_elements = new int[n];
	this->_resizeFactor = n;
}

Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
	this->_capacity = 0;
	this->_size = 0;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return this->_size == 0;
}

void Vector::push_back(const int& val)
{
	// make sure there is enough space and than add
	this->reserve(this->_size + 1);
	this->_elements[this->_size++] = val;
}

int Vector::pop_back()
{
	int lastElement = POP_ERROR;
	if (this->_size == 0) {
		std::cerr << "error: pop from empty vector\n";
	}
	else {
		lastElement = this->_elements[--this->_size];
	}
	return lastElement;
}

void Vector::reserve(int n)
{
	int expansion = 0;
	int* temp = nullptr;
	int i = 0;

	if (this->_capacity < n) {
		// not enough space
		expansion = n - this->_capacity;
		if (expansion % this->_resizeFactor != 0) {	// not a multiple of resize factor
			expansion = (expansion / this->_resizeFactor + 1) * this->_resizeFactor;	// round up
		}
		// resize
		this->_capacity += expansion;
		temp = this->_elements;
		this->_elements = new int[this->_capacity];
		// move values from old array to new array
		for (i = 0; i < this->_size; i++) {
			this->_elements[i] = temp[i];
		}

		delete[] temp;
	}
}

void Vector::resize(int n)
{
	reserve(n);
	this->_size = n;
}

void Vector::resize(int n, const int& val)
{
	int i = 0;
	int oldSize = this->_size;

	reserve(n);
	this->_size = n;

	// set new values
	for (i = oldSize; i < n; i++) {
		this->_elements[i] = val;
	}
}

void Vector::assign(int val)
{
	int i = 0;
	// set all values
	for (i = 0; i < this->_size; i++) {
		this->_elements[i] = val;
	}
}

Vector::Vector(const Vector& other)
{
	this->_elements = nullptr;
	*this = other;
}

Vector& Vector::operator=(const Vector& other)
{
	int i = 0;

	// shallow copy regular properties
	this->_size = other._size;
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;

	// first free previous memory
	if (this->_elements != nullptr) {
		delete[] this->_elements;
	}
	// deep copy _elements
	this->_elements = new int[this->_capacity];
	for (i = 0; i < this->_size; i++) {
		this->_elements[i] = other._elements[i];
	}

	return *this;
}

int& Vector::operator[](int n) const
{
	if (n < 0 || this->_size <= n) {
		// index out of range
		std::cerr << "Index out of range!\n";
		n = 0;
	}
	
	return this->_elements[n];
}

Vector Vector::operator+(const Vector& other) const
{
	int i = 0;
	Vector temp(RESIZE_FACTOR);
	// add values
	for (i = 0; i < this->_size && i < other.size(); i++) {
		temp.push_back((*this)[i] + other[i]);
	}

	return temp;
}

Vector& Vector::operator+=(const Vector& other)
{
	*this = *this + other;
	return *this;
}

Vector Vector::operator-(const Vector& other) const
{
	int i = 0;
	Vector temp(RESIZE_FACTOR);
	// add values
	for (i = 0; i < this->_size && i < other.size(); i++) {
		temp.push_back((*this)[i] - other[i]);
	}

	return temp;
}

Vector& Vector::operator-=(const Vector& other)
{
	*this = *this - other;
	return *this;
}

std::ostream& operator<<(std::ostream& output, const Vector& vector)
{
	output << "{ ";
	int i = 0;

	for (i = 0; i < vector.size() - 1; i++) {
		output << vector[i] << ", ";
	}
	output << vector[vector.size() - 1] << " }";

	return output;
}
